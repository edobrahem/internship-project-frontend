/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  safelist: [
    "text-2xl",
    "text-3xl",
    {
      pattern: /(bg|text|border-t)-[a-z]*-[\d]{2,}/g,
      variants: ["lg", "hover", "focus", "lg:hover"],
    },
  ],
  theme: {
    extend: {},
  },
  plugins: [],
};

import { TfiTrash } from "react-icons/tfi";
import { BiEditAlt } from "react-icons/bi";
import { ComponentProps, FC } from "react";
import { Employee } from "../utils/Interfaces";

export type EmpTableProp = ComponentProps<"table"> & {
  onDelete: (id?: number) => void;
  onUpdate: (id?: number) => void;
  data: Employee[];
};

const EmployeeTable: FC<EmpTableProp> = ({ onUpdate, data, onDelete }) => {
  return (
    <table className="divide-y divide-gray-300 mx-auto ">
      <thead className="bg-black ">
        <tr>
          <th className="table-head">Nom</th>
          <th className="table-head">Prenom</th>
          <th className="table-head">CIN</th>
          <th className="table-head">Adresse Email</th>
          <th className="table-head">Date De Création</th>
          <th className="table-head">Date De Derniére Modification</th>
          <th className="table-head">Modifier</th>
          <th className="table-head">Supprimer</th>
        </tr>
      </thead>
      <tbody className="bg-white divide-y divide-gray-500">
        {data?.map((emp) => {
          const createdAtDate = emp.attributes.createdAt!.substring(0, 10);
          const createdAtTime = emp.attributes.createdAt!.substring(11, 19);
          const updatedAtDate = emp.attributes.updatedAt!.substring(0, 10);
          const updatedAtTime = emp.attributes.updatedAt!.substring(11, 19);
          return (
            <tr
              key={emp.id}
              className="transition-colors ease-in-out duration-300 bg-gray-50 whitespace-nowrap hover:bg-blue-50 "
            >
              <td className="table-data">{emp.attributes.nomEmploye}</td>
              <td className="table-data">{emp.attributes.prenomEmploye}</td>
              <td className="table-data">{emp.attributes.cin}</td>
              <td className="table-data">{emp.attributes.mailEmploye}</td>
              <td className="table-data">
                {createdAtDate} à {createdAtTime}
              </td>
              <td className="table-data">
                {updatedAtDate} à {updatedAtTime}
              </td>
              <td className="table-data">
                <button
                  type="button"
                  className="ed-btn"
                  onClick={() => onUpdate(emp.id)}
                >
                  <BiEditAlt />
                </button>
              </td>
              <td className="table-data">
                <button
                  type="button"
                  className="del-btn"
                  onClick={() => onDelete(emp.id)}
                >
                  <TfiTrash />
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default EmployeeTable;

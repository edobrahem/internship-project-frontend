/* eslint-disable react/self-closing-comp */
import classNames from "classnames";
import { ComponentProps, FC } from "react";
import { GrAdd } from "react-icons/gr";
import { Droppable } from "react-beautiful-dnd";

import { Task } from "../utils/Interfaces";
import TaskBody from "./TaskBody";

export type ColumnProps = ComponentProps<"div"> & {
  title: string;
  color: string;
  containerName: string;
  taskArray: Task[];
};

const TaskColumn: FC<ColumnProps> = ({
  title,
  color,
  containerName,
  taskArray,
}) => {
  return (
    <div
      className={`rounded-lg border-2 border-blue-300 border-t-8 p-2 border-t-${color}-500 bg-${color}-50 h-fit`}
    >
      <div
        className={classNames(
          `flex justify-between  rounded-lg p-2 bg-${color}-100`,
        )}
      >
        <p className="font-semibold">{title}</p>
        <button
          type="button"
          className={classNames(
            `transition-colors ease-in-out duration-300 p-2 rounded-lg hover:bg-${color}-400  `,
          )}
        >
          <GrAdd />
        </button>
      </div>

      <Droppable droppableId={containerName}>
        {(provided) => (
          <div
            ref={provided.innerRef}
            {...provided.droppableProps}
            className={classNames(
              `rounded-lg bg-${color}-100 mt-4 p-2 min-h-[50vh] overflow-y-hidden overflow-x-hidden`,
            )}
          >
            {taskArray.map((T, i) => {
              const createdAtDate = T.attributes.createdAt!.substring(5, 10);
              const createdAtTime = T.attributes.createdAt!.substring(11, 15);
              return (
                <TaskBody
                  taskIndex={i}
                  taskId={T.id}
                  key={T.id}
                  title={T.attributes.titre}
                  date={`${createdAtDate} à ${createdAtTime}`}
                />
              );
            })}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </div>
  );
};
export default TaskColumn;

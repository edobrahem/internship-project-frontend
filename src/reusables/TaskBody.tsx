import { ComponentProps, FC } from "react";
import { MdNotes } from "react-icons/md";
import { Draggable } from "react-beautiful-dnd";

export type TaskProps = ComponentProps<"div"> & {
  title: string;
  date: string;
  taskId: number;
  taskIndex: number;
};

const TaskBody: FC<TaskProps> = ({ title, date, taskId, taskIndex }) => {
  return (
    <Draggable draggableId={`${taskId}`} index={taskIndex}>
      {(provided) => (
        <div
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
          className=" w-auto bg-white rounded-lg p-2 my-4  "
        >
          <div className="flex justify-between h-fit">
            <p className="text-sm">{title}</p>
            <button
              type="button"
              className="row-span-2 flex justify-end px-4 border-2 border-slate-200 rounded-lg hover:border-slate-400"
            >
              <MdNotes />
            </button>
          </div>

          <p className="text-xs mt-2">{date}</p>
        </div>
      )}
    </Draggable>
  );
};

export default TaskBody;

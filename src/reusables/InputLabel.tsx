import { ComponentProps, FC } from "react";

export type LabelProps = ComponentProps<"label"> & {
  htmlfor: string;
  message: string;
};
const EmpFormLabel: FC<LabelProps> = ({ htmlfor, message }) => {
  return (
    <label
      htmlFor={htmlfor}
      className="absolute text-sm text-gray-500  duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white  px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
    >
      {message}
    </label>
  );
};

export default EmpFormLabel;

import React, { ComponentProps, FC, Ref } from "react";

export type InputProps = ComponentProps<"input"> & {
  id: string;
  type: string;
  ref: Ref<HTMLInputElement>;
};

const TextInput: FC<InputProps> = React.forwardRef(
  ({ id, type: inputType, ...props }, ref) => {
    return (
      <input
        {...props}
        ref={ref}
        type={inputType}
        id={id}
        className="flex px-2.5 pb-2.5 pt-4 w-full  text-sm text-gray-900 bg-transparent rounded-lg border-2 border-l-neutral-900 appearance-none dark:text-black dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer "
      />
    );
  },
);

TextInput.displayName = "custom input";

export default TextInput;

import { ComponentProps, FC } from "react";
import DelayedButton from "./DelayedButton";

export type ConfirmProps = ComponentProps<"div"> & {
  toggle: (id?: number) => void;
  onDelete: (id: number) => Promise<void>;
  delId: number;
};
const ConfirmBox: FC<ConfirmProps> = ({ toggle, onDelete, delId }) => {
  return (
    <div className=" bg-black fixed w-full h-screen top-0 left-0 bg-opacity-20 flex justify-center ">
      <div
        className="bg-white relative h-fit max-w-xl rounded-xl p-5 overflow-auto border-4 border-blue-500  m-10 flex flex-col"
        style={{ width: "70%", marginTop: "calc(100vh - 85vh - 20px)" }}
      >
        <div className="grid grid-cols-2 gap-6 justify-center">
          <p className="col-span-2 w-full flex text-lg font-semibold justify-center mb-8">
            Êtes-vous sûr de vouloir supprimer Cet Employé ?
          </p>
          <button
            className="bg-blue-700 w-auto rounded-lg px-4 py-2 text-white "
            type="button"
            onClick={() => toggle()}
          >
            Annuler
          </button>
          <DelayedButton
            type="button"
            onClick={() => onDelete(delId)}
            msg="Oui"
          />
        </div>
      </div>
    </div>
  );
};

export default ConfirmBox;

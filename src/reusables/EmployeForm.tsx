import { ComponentProps, FC } from "react";
import { useForm } from "react-hook-form";
import { useAddEmploye, useEditEmploye } from "../Services/EmployeeService";
import { Employee } from "../utils/Interfaces";
import EmpFormLabel from "./InputLabel";
import TextInput from "./TextInput";

export type FormProps = ComponentProps<"form"> & {
  closePopup: () => void;
  ButtonBool: boolean;
  employee: Employee | undefined;
};

const EmployeForm: FC<FormProps> = ({ closePopup, employee, ButtonBool }) => {
  const { mutate: addEmploye } = useAddEmploye();
  const { mutate: editEmploye } = useEditEmploye();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      dateNEmploye: null,
    },
    values: employee?.attributes,
  });

  const submit = (data: any) => {
    if (employee) {
      editEmploye({ data, id: employee.id });
      closePopup();
    } else {
      addEmploye(data);
      closePopup();
    }
  };
  return (
    <div className="mx-8 my-10 flex items-center justify-center h-full  rounded-lg">
      <form
        className="grid grid-cols-2 gap-6 justify-items-stretch "
        onSubmit={handleSubmit((data) => submit(data))}
      >
        <div className="relative">
          <TextInput
            {...register("nomEmploye", {
              required: "Ce Champ Est Obligatoire",
              maxLength: {
                value: 10,
                message: "Le nom ne peut pas avoir plus que 10 caractéres",
              },
              minLength: {
                value: 3,
                message: "Le nom doit avoir au moins 3 caractéres",
              },
              pattern: {
                value: /[A-Z][a-z]*/,
                message: "Le nom doit commencer avec une majuscule",
              },
            })}
            type="text"
            id="nomEmploye"
            placeholder="..."
          />
          <EmpFormLabel htmlfor="nomEmploye" message="Nom" />

          <p className="text-red-600 bg-red-300 rounded-lg px-2 mt-2">
            {errors.nomEmploye?.message}
          </p>
        </div>
        <div className="relative ">
          <TextInput
            {...register("prenomEmploye", {
              required: "Ce Champ Est Obligatoire",
              maxLength: {
                value: 10,
                message: "Le prénom ne peut pas avoir plus que 10 caractéres",
              },
              minLength: {
                value: 3,
                message: "Le prénom doit avoir au moins 3 caractéres",
              },
              pattern: {
                value: /[A-Z][a-z]*/,
                message: "Le prénom doit commencer avec une majuscule",
              },
            })}
            type="text"
            id="prenomEmploye"
            placeholder="..."
          />
          <EmpFormLabel htmlfor="prenomEmploye" message="Prenom" />

          <p className="text-red-600 bg-red-300 rounded-lg px-2 mt-2">
            {errors.prenomEmploye?.message}
          </p>
        </div>
        <div className="relative ">
          <TextInput
            {...register("cin", {
              required: "Ce champ est obligatoire",
              maxLength: {
                value: 8,
                message: "Le CIN doit contenir 8 chiffres",
              },
              minLength: {
                value: 8,
                message: "Le CIN doit contenir 8 chiffres",
              },
              pattern: {
                value: /[0-9]{8}/,
                message: "Le CIN doit etre composé par des chiffres uniquement",
              },
            })}
            type="text"
            id="cinEmploye"
            placeholder="..."
          />
          <EmpFormLabel htmlfor="cinEmploye" message="CIN" />
          <p className="text-red-600 bg-red-300 rounded-lg px-2 mt-2">
            {errors.cin?.message}
          </p>
        </div>
        <div className="relative ">
          <TextInput
            {...register("dateNEmploye")}
            type="date"
            id="dateNEmploye"
            placeholder="..."
          />
          <EmpFormLabel htmlfor="dateNEmploye" message="Date Naissance" />
        </div>
        <div className="relative col-span-2 ">
          <TextInput
            {...register("mailEmploye", {
              required: "Ce Champ Est Obligatoire",
              maxLength: {
                value: 35,
                message:
                  "L'adresse email ne peut pas avoir plus que 25 caractéres",
              },
              minLength: {
                value: 10,
                message: "L'adresse email doit avoir au moins 10 caractéres",
              },
              pattern: {
                value: /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/,
                message: "L'adresse email n'est pas valide",
              },
            })}
            type="mail"
            id="emailEmploye"
            placeholder="..."
          />
          <EmpFormLabel htmlfor="emailEmploye" message="Adresse E-mail" />
          <p className="text-red-600 bg-red-300 rounded-lg px-2 mt-2 text-center">
            {errors.mailEmploye?.message}
          </p>
        </div>
        <button
          type="button"
          className=" text-white border-1 bg-red-600  rounded-lg py-2 px-4  max-w-fit"
          onClick={closePopup}
        >
          Annuler
        </button>
        <button
          type="submit"
          className=" text-white border-1 bg-green-600  rounded-lg py-2 px-4  max-w-fit justify-self-end"
        >
          {ButtonBool ? "Ajouter" : "Mettre A jour"}
        </button>
      </form>
    </div>
  );
};

export default EmployeForm;

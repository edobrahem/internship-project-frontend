import { ComponentProps, FC, useState } from "react";

export type DelayedButtonProps = ComponentProps<"button"> & {
  msg: string;
};

const DelayedButton: FC<DelayedButtonProps> = ({ msg, ...props }) => {
  const [isDis, setDis] = useState(true);
  const [color, setColor] = useState("bg-red-200");
  setTimeout(() => {
    setDis(false);
    setColor("bg-red-600");
  }, 5000);
  return (
    <button
      className={` w-auto rounded-lg px-4 py-2 text-white ${color}`}
      onClick={props.onClick}
      type="button"
      disabled={isDis}
    >
      {msg}
    </button>
  );
};

export default DelayedButton;

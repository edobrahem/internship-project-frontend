const employeesUri = process.env.REACT_APP_EMPLOYEE_URI;
const apiToken = process.env.REACT_APP_API_TOKEN;
const taskUri = process.env.REACT_APP_TASK_URI;
export { apiToken, employeesUri, taskUri };

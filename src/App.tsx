import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";
import EmployeeList from "./pages/EmployeeListe";
import NotFound from "./pages/NotFound";
import AboutUs from "./pages/AboutUs";
import NavBar from "./components/NavBar";
import Kanban from "./pages/Kanban";

function App() {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route path="/" element={<Kanban />} />
          <Route path="/AboutUs" element={<AboutUs />} />
          <Route path="/management" element={<EmployeeList />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </QueryClientProvider>
  );
}

export default App;

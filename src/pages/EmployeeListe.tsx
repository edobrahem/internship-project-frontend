import { useQuery } from "react-query";
import { SiAddthis } from "react-icons/si";
import { useEffect, useState } from "react";
import { type Employee, type ApiResponse } from "../utils/Interfaces";
import { useDeleteEmploye, getAllEmployees } from "../Services/EmployeeService";
import AddFormPopup from "../components/AddFormPopup";
import EditFormPopup from "../components/EditFormPopup";
import EmployeeTable from "../reusables/EmployeeTable";
import ConfirmBox from "../reusables/ConfirmBox";

function EmployeList() {
  const [delId, setDelid] = useState(0);
  const [editId, setEditId] = useState(0);
  const [isAddOpen, setIsAddOpen] = useState(false);
  const [isEditOpen, setIsEditOpen] = useState(false);
  const [isConfirmOpen, setConfirm] = useState(false);
  const { data, error, isLoading } = useQuery<ApiResponse<Employee[]>>(
    "employeeData",
    getAllEmployees,
  );
  const { mutate: deleteEmployee } = useDeleteEmploye();
  const [employeeArray, setEmployeeArray] = useState<Employee[]>([]);

  const toggleAddForm = () => {
    setIsAddOpen(!isAddOpen);
  };

  const toggleEditForm = (id?: number) => {
    if (id !== undefined) {
      setEditId(id);
    }
    setIsEditOpen(!isEditOpen);
  };

  const toggleConfirm = (id?: number) => {
    if (id !== undefined) {
      setDelid(id);
    }
    setConfirm(!isConfirmOpen);
  };

  const handleDelete = async (id: number) => {
    deleteEmployee(id);
    toggleConfirm();
  };

  useEffect(() => {
    if (data?.data != null) {
      setEmployeeArray(data?.data);
    }
  }, [data?.data]);

  if (error)
    return <div className="flex justify-center">an error occured....</div>;
  if (isLoading)
    return <div className="flex justify-center">Currently Loading ...</div>;

  return (
    <>
      <button type="button" className="add-btn" onClick={toggleAddForm}>
        <SiAddthis />
      </button>
      <div className="container justify-center  my-20 mx-auto flex flex-col ">
        <div className="p-3 border-b border-gray-400 shadow-md justify-center bg-blue-50 rounded-lg">
          <h1 className="flex justify-center text-3xl font-extrabold mb-5">
            Liste Des Employés
          </h1>
          <div className="overflow-x-auto">
            <EmployeeTable
              onDelete={toggleConfirm}
              onUpdate={toggleEditForm}
              data={employeeArray}
            />
          </div>
        </div>
      </div>
      {isEditOpen && <EditFormPopup handleclose={toggleEditForm} id={editId} />}
      {isAddOpen && <AddFormPopup handleclose={toggleAddForm} />}
      {isConfirmOpen && (
        <ConfirmBox
          toggle={toggleConfirm}
          onDelete={handleDelete}
          delId={delId}
        />
      )}
    </>
  );
}

export default EmployeList;

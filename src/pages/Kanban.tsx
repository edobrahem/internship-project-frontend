/* eslint-disable default-case */
/* eslint-disable no-useless-return */
/* eslint-disable react/self-closing-comp */
import { useEffect, useState } from "react";
import { DragDropContext } from "react-beautiful-dnd";
import { useQuery } from "react-query";
import Sidenav from "../components/SideNav";
import TaskColumn from "../reusables/TaskColumn";
import { getAllTasks, putTask } from "../Services/TaskService";
import { ApiResponse, Task } from "../utils/Interfaces";

const filterArrayByStatus = (array: Task[], status: string) => {
  const newArray = array.filter((task) => task.attributes.etat === status);
  return newArray;
};

function Kanban() {
  const { data } = useQuery<ApiResponse<Task[]>>(`taskData`, getAllTasks);

  const [todoArray, setTodoArray] = useState<Task[]>([]);
  const [progressArray, setProgressArray] = useState<Task[]>([]);
  const [reviewArray, setReviewArray] = useState<Task[]>([]);
  const [doneArray, setDoneArray] = useState<Task[]>([]);

  const setters = [
    setTodoArray,
    setProgressArray,
    setReviewArray,
    setDoneArray,
  ];
  const columnIds = ["aFaire", "enCours", "enRevision", "accomplis"];
  const mapOfArrays: Map<string, Task[]> = new Map([
    ["aFaire", todoArray],
    ["enCours", progressArray],
    ["enRevision", reviewArray],
    ["accomplis", doneArray],
  ]);

  const onDragEnd = (result: any) => {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }
    if (destination.droppableId !== source.droppableId) {
      const draggedTask = mapOfArrays.get(source.droppableId)?.at(source.index);
      const sourceArray = mapOfArrays.get(source.droppableId);
      const destinationArray = mapOfArrays.get(destination.droppableId);
      const sourceSetterIndex = columnIds.indexOf(source.droppableId);
      const destinationSetterIndex = columnIds.indexOf(destination.droppableId);

      sourceArray?.splice(source.index, 1);
      destinationArray?.splice(destination.index, 0, draggedTask!);

      setters[sourceSetterIndex](sourceArray!);
      setters[destinationSetterIndex](destinationArray!);

      draggedTask!.attributes.etat = destination.droppableId;

      putTask(draggedTask!.attributes, draggableId);
    }
    if (
      destination.droppableId === source.droppableId &&
      destination.index !== source.index
    ) {
      const correspondingSetterIndex = columnIds.indexOf(
        destination.droppableId,
      );

      const draggedTask = mapOfArrays.get(source.droppableId)?.at(source.index);
      const newArray = Array.from(mapOfArrays.get(destination.droppableId)!);
      newArray.splice(source.index, 1);

      newArray.splice(destination.index, 0, draggedTask!);

      setters[correspondingSetterIndex](newArray);
    }
  };

  useEffect(() => {
    if (data?.data) {
      setTodoArray(filterArrayByStatus(data.data, "aFaire"));
      setProgressArray(filterArrayByStatus(data.data, "enCours"));
      setReviewArray(filterArrayByStatus(data.data, "enRevision"));
      setDoneArray(filterArrayByStatus(data.data, "accomplis"));
    }
  }, [data?.data]);
  return (
    <>
      <Sidenav />
      <h1 className="flex justify-center text-3xl font-extrabold my-8">
        Tableau Des Taches
      </h1>
      <div className="grid  grid-cols-4 gap-3 mx-20 my-10 ">
        <DragDropContext onDragEnd={onDragEnd}>
          <TaskColumn
            taskArray={todoArray}
            color="blue"
            title="A Faire"
            containerName="aFaire"
          ></TaskColumn>
          <TaskColumn
            taskArray={progressArray}
            color="yellow"
            title="En Cours"
            containerName="enCours"
          ></TaskColumn>
          <TaskColumn
            taskArray={reviewArray}
            color="red"
            title="En Revision"
            containerName="enRevision"
          ></TaskColumn>
          <TaskColumn
            taskArray={doneArray}
            color="green"
            title="Accomplis"
            containerName="accomplis"
          ></TaskColumn>
        </DragDropContext>
      </div>
    </>
  );
}

export default Kanban;

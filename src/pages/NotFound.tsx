function NotFound() {
  return (
    <div className="flex justify-center items-center h-screen">
      <h1 className="text-8xl ">ERROR 404: </h1>
      <br />
      <h2 className="text-4xl ">The page you're looking for cannot be found</h2>
    </div>
  );
}

export default NotFound;

import { apiToken } from "../Config";

/* eslint-disable @typescript-eslint/return-await */
const sendRequest = async <T>(
  path: string,
  config?: RequestInit,
): Promise<T> => {
  const init = config
    ? {
        ...config,
        body: config.body
          ? JSON.stringify({ data: config.body.valueOf() })
          : null,
        headers: {
          ...config.headers,
          Authorization: `${apiToken}`,
          "Content-Type": "application/json",
        },
      }
    : {
        headers: {
          Authorization: `${apiToken}`,
          "Content-Type": "application/json",
        },
      };

  const request = new Request(path, init);
  const response = await fetch(request);

  if (!response.ok) {
    throw new Error(
      `Status Code : ${response.status}, message : ${response.statusText}`,
    );
  }

  return await response.json().catch(() => ({}));
};

export default sendRequest;

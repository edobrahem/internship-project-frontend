import { useMutation, useQueryClient } from "react-query";
import { UpdateVariables } from "../utils/Interfaces";
import { employeesUri } from "../Config";
import sendRequest from "./SendRequest";

const deleteEmployee = (id: number | undefined) => {
  const res = sendRequest<any>(`${employeesUri}/${id}`, {
    method: "delete",
  });
  return res;
};
const getAllEmployees = () => {
  const res = sendRequest<any>(`${employeesUri}`, {
    method: "get",
  });

  return res;
};

const getEmployeeNames = () => {
  const res = sendRequest<any>(
    `${employeesUri}?fields[0]=nomEmploye&fields[1]=prenomEmploye`,
    {
      method: "get",
    },
  );

  return res;
};

const getEmployee = (id: number) => {
  const res = sendRequest<any>(`${employeesUri}/${id}`, {
    method: "get",
  });
  return res;
};

const postEmployee = (data: any) => {
  const res = sendRequest<any>(`${employeesUri}`, {
    body: data,
    method: "post",
  });
  return res;
};

const putEmployee = (data: any, id: number) => {
  const res = sendRequest<any>(`${employeesUri}/${id}`, {
    body: data,
    method: "put",
  });
  return res;
};

const useAddEmploye = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: postEmployee,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["employeeData"] });
    },
  });
};

const useEditEmploye = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: ({ data, id }: UpdateVariables) => putEmployee(data, id),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["employeeData"] });
    },
  });
};

const useDeleteEmploye = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: deleteEmployee,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["employeeData"] });
    },
  });
};
export {
  getAllEmployees,
  getEmployee,
  useAddEmploye,
  useDeleteEmploye,
  useEditEmploye,
  getEmployeeNames,
};

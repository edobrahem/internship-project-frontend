import { useMutation, useQueryClient } from "react-query";
import { taskUri } from "../Config";
import { UpdateVariables } from "../utils/Interfaces";
import sendRequest from "./SendRequest";

const deleteTask = (id: number | undefined) => {
  const res = sendRequest<any>(`${taskUri}/${id}`, {
    method: "delete",
  });
  return res;
};
const getAllTasks = () => {
  const res = sendRequest<any>(`${taskUri}`, {
    method: "get",
  });
  return res;
};

const getTasksByType = async (typeParam: string) => {
  const res = sendRequest<any>(`${taskUri}?filters[etat][$eqi]=${typeParam}`, {
    method: "get",
  });

  return res;
};

const getTask = async (id: number) => {
  const res = sendRequest<any>(`${taskUri}/${id}`, {
    method: "get",
  });
  return res;
};

const postTask = (data: any) => {
  const res = sendRequest<any>(`${taskUri}`, {
    body: data,
    method: "post",
  });
  return res;
};

const putTask = (data: any, id: number) => {
  const res = sendRequest<any>(`${taskUri}/${id}`, {
    body: data,
    method: "put",
  });
  return res;
};

const useAddTask = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: postTask,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["taskData"] });
    },
  });
};

const useEditTask = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: ({ data, id }: UpdateVariables) => putTask(data, id),
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ["taskData"],
      });
    },
  });
};

const useDeleteTask = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: deleteTask,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["taskData"] });
    },
  });
};

export {
  getTask,
  getAllTasks,
  useAddTask,
  useDeleteTask,
  useEditTask,
  getTasksByType,
  putTask,
};

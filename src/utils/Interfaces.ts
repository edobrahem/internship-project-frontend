interface ApiResponse<T> {
  data: T;
}

interface Employee {
  id: number;
  attributes: {
    prenomEmploye: string;
    nomEmploye: string;
    cin: string;
    mailEmploye: string;
    dateNEmploye?: string | null;
    createdAt?: string;
    updatedAt?: string;
  };
}

interface Task {
  id: number;
  attributes: {
    titre: string;
    description: string;
    etat: string;
    responsable: number;
    createur: number;
    createdAt: string;
    updatedAt: string;
  };
}

interface ButtonProp {
  handleclose: () => void;
}

interface EditButtonProp {
  handleclose: () => void;
  id: number;
}

interface UpdateVariables {
  data: any;
  id: number;
}

export type {
  Employee,
  ApiResponse,
  ButtonProp,
  EditButtonProp,
  UpdateVariables,
  Task,
};

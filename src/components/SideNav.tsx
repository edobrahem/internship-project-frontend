import { useEffect, useState } from "react";
import { BiUserCircle } from "react-icons/bi";
import { RxDoubleArrowRight, RxDoubleArrowLeft } from "react-icons/rx";
import { useQuery } from "react-query";
import { getEmployeeNames } from "../Services/EmployeeService";
import { ApiResponse, Employee } from "../utils/Interfaces";

const SideNav = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [employeeNames, setEmployeeNames] = useState<Employee[]>([]);

  const toggleOpen = () => {
    setIsOpen(!isOpen);
  };
  const { data } = useQuery<ApiResponse<Employee[]>>(
    "employeeFullNames",
    getEmployeeNames,
  );

  useEffect(() => {
    if (data?.data) {
      setEmployeeNames(data.data);
    }
  }, [data?.data]);
  return (
    <div className={` ${isOpen ? "sidenav" : "sidenavClosed"}`}>
      <button type="button" onClick={toggleOpen} className="menubutton ">
        {isOpen ? (
          <RxDoubleArrowLeft className="text-black" />
        ) : (
          <RxDoubleArrowRight className="text-black" />
        )}
      </button>
      {isOpen && (
        <p className="flex justify-center text-lg">Liste Des Collaborateurs</p>
      )}
      <div className="mt-6 mb-12">
        {employeeNames.map((emp) => {
          return (
            <div
              key={emp.id}
              className={`transition-opacity ease-in-out duration-500 m-4 p-3 flex justify-between w-auto h-fit rounded-lg border-4 bg-neutral-100 border-gray-300 text-2xl ${
                isOpen ? "opacity-100" : "opacity-0"
              }`}
            >
              <BiUserCircle />
              <p className="text-sm font-semibold">
                {emp.attributes.nomEmploye} {emp.attributes.prenomEmploye}
              </p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default SideNav;

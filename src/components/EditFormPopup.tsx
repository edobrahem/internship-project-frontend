import { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { getEmployee } from "../Services/EmployeeService";
import EmpForm from "../reusables/EmployeForm";
import {
  ApiResponse,
  Employee,
  type EditButtonProp,
} from "../utils/Interfaces";

const EditFormPopup: React.FC<EditButtonProp> = ({ handleclose, id }) => {
  const { data, isLoading } = useQuery<ApiResponse<Employee>>("employe", () =>
    getEmployee(id),
  );

  const [employee, setemployee] = useState<Employee | undefined>();
  useEffect(() => {
    if (data?.data) {
      setemployee(data?.data);
    }
  }, [data?.data]);
  return (
    <div className=" bg-black fixed w-full h-screen top-0 left-0 bg-opacity-20 flex justify-center transition ease-in-out delay-150 ">
      <div
        className="bg-white relative h-fit max-w-xl rounded-xl p-5 overflow-auto border-4 border-blue-500  m-10 flex flex-col"
        style={{ width: "70%", marginTop: "calc(100vh - 85vh - 20px)" }}
      >
        <h1 className=" flex justify-center font-extrabold text-2xl">
          Formualire Modification Employé
        </h1>
        {isLoading ? (
          <div className="text-2xl justify-center">Pending...</div>
        ) : (
          <EmpForm
            closePopup={handleclose}
            employee={employee}
            ButtonBool={false}
          />
        )}
      </div>
    </div>
  );
};

export default EditFormPopup;

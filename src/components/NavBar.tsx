import { BsKanbanFill } from "react-icons/bs";
import { NavLink } from "react-router-dom";
import { AiOutlineLogout } from "react-icons/ai";

function NavBar() {
  return (
    <nav className=" md:h-20 p2 bg-blue-900 max-w-8xl flex justify-between shadow-lg items-center sticky top-0 z-50 ">
      <div className="flex-1">
        <NavLink className="nav-link " to="/management">
          Liste Des Employées
        </NavLink>
        <NavLink className="nav-link " to="/">
          Tableau Des Taches
        </NavLink>
        <NavLink className="nav-link " to="/AboutUs">
          Contactez Nous
        </NavLink>
      </div>
      <div className="flex justify-between content-center">
        <BsKanbanFill className="text-5xl text-white " />
        <p className="font-extrabold text-white text-3xl">HexaBoard</p>
      </div>
      <div className=" flex flex-1 justify-end items-center text-white mr-2">
        <AiOutlineLogout className="text-5xl text-white" />
        LogOut
      </div>
    </nav>
  );
}

export default NavBar;

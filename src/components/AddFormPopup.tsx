import { ButtonProp } from "../utils/Interfaces";
import EmpForm from "../reusables/EmployeForm";

const AddFormPopup: React.FC<ButtonProp> = ({ handleclose }) => {
  return (
    <div className=" bg-black fixed w-full h-screen top-0 left-0 bg-opacity-20 flex justify-center transition ease-in-out delay-150 ">
      <div
        className="bg-white relative h-fit max-w-xl rounded-xl p-5 overflow-auto border-4 border-blue-500  m-10 flex flex-col "
        style={{ width: "70%", marginTop: "calc(100vh - 85vh - 20px)" }}
      >
        <h1 className=" flex justify-center font-extrabold text-2xl">
          Formualire Ajout Employé
        </h1>

        <EmpForm closePopup={handleclose} employee={undefined} ButtonBool />
      </div>
    </div>
  );
};

export default AddFormPopup;
